import './App.css';
import NavbarTop from "../src/Components/NavbarTop";
import SlideShow from "../src/Components/SlideShow";
import LatestNews from "../src/Components/LatestNews";
import FeaturedVideos from "../src/Components/FeaturedVideos";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <NavbarTop />
      <SlideShow />
      <LatestNews />
      <FeaturedVideos />
    </div>
  );
}

export default App;
