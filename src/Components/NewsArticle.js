import React from "react";
import { Row, Col } from "react-bootstrap";
import styled from "styled-components"

const Headline = styled.div`
font-weight: bold;
font-size: 16pt;
`;

const Date = styled.div`
font-size: 12pt
`;

export function NewsArticle(props) {
    const image = props.image;
    const isRightText = props.isRightText || false;
    const dateText = props.date;
    const headlineText = props.headline;

    return <>
        <Row>
            <Col xs="12" md="6">
                <img src={image} style={{ height: "100%", width: "100%" }} alt="" />
            </Col>
            <Col style={{ order: isRightText ? 1 : -1 }} xs={false} md="6">
                <Row>
                    <Date>{dateText}</Date>
                </Row>
                <Row>
                    <Headline>{headlineText}</Headline>
                </Row>
            </Col>
        </Row>
    </>
}