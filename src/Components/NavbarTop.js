import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'

const navStyle = {
    color: "white",
    paddingLeft: "2em",
};

function NavbarTop() {
    return (
        <div>
            {
                <Navbar bg="black" expand="lg">
                    <Container>
                        <Navbar.Brand href="#home" style={{ color: "white", paddingRight: "18em" }}>UBISOFT</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <Nav.Link href="#uplay+" style={navStyle}>UPLAY+</Nav.Link>
                                <Nav.Link href="#store" style={navStyle}>STORE</Nav.Link>
                                <Nav.Link href="#moregames" style={navStyle}>MORE GAMES</Nav.Link>
                                <Nav.Link href="#news" style={navStyle}>NEWS</Nav.Link>
                                <Nav.Link href="#forums" style={navStyle}>FORUMS</Nav.Link>
                                <Nav.Link href="#support" style={navStyle}>SUPPORT</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            }
        </div >
    )
}

export default NavbarTop