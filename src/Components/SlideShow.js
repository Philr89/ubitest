import React from 'react'
import Button from 'react-bootstrap/Button'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Card from 'react-bootstrap/Card'
import Sample1 from '../Assets/explore-more-games-sample-1.jpg'
import Sample2 from '../Assets/explore-more-games-sample-2.jpg'
import Sample3 from '../Assets/explore-more-games-sample-3.jpg'
import Sample4 from '../Assets/explore-more-games-sample-4.jpg'
import Sample5 from '../Assets/explore-more-games-sample-5.jpg'
import Sample6 from '../Assets/explore-more-games-sample-6.jpg'
import ps4Logo from '../Assets/ps4.svg'
import switchLogo from '../Assets/switch.svg'
import windowsLogo from '../Assets/windows.svg'
import xboxLogo from '../Assets/xbox.svg'
import Container from 'react-bootstrap/Container'

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 6,
        slidesToSlide: 6,
    },
    mobile: {
        breakpoint: { max: 864, min: 0 },
        items: 3,
        slidesToSlide: 3
    }
};

function SlideShow() {
    return (
        <div>
            {
                <><div style={{ backgroundColor: "rgb(239, 245, 243)" }}>
                    <Container>
                        <div style={{ textAlign: "center", padding: "2em 0 2em 0" }}><h1>EXPLORE MORE GAMES</h1></div>
                        <Carousel responsive={responsive}>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample1} />
                                    <Card.Body>
                                        <Card.Title>Watch Dogs: Legion</Card.Title>
                                        <Card.Text>
                                            <p>2021</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample2} />
                                    <Card.Body>
                                        <Card.Title>Immortals: Fenyx Rising</Card.Title>
                                        <Card.Text>
                                            <p>COMING SOON</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img><img src={switchLogo} alt="Switch"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample3} />
                                    <Card.Body>
                                        <Card.Title>Riders Republic</Card.Title>
                                        <Card.Text>
                                            <p>COMING SOON</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample4} />
                                    <Card.Body>
                                        <Card.Title>Scott Pilgrim vs. The World: The Game</Card.Title>
                                        <Card.Text>
                                            <p>PLAY NOW</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img><img src={switchLogo} alt="Switch"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample5} />
                                    <Card.Body>
                                        <Card.Title>Prince of Persia: The Sands of Time Remake</Card.Title>
                                        <Card.Text>
                                            <p>2021</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample6} />
                                    <Card.Body>
                                        <Card.Title>AGOS: A Game of Space</Card.Title>
                                        <Card.Text>
                                            <p>COMING SOON</p>
                                            <img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample5} />
                                    <Card.Body>
                                        <Card.Title>Prince of Persia: The Sands of Time Remake</Card.Title>
                                        <Card.Text>
                                            <p>2021</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample2} />
                                    <Card.Body>
                                        <Card.Title>Immortals: Fenyx Rising</Card.Title>
                                        <Card.Text>
                                            <p>COMING SOON</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img><img src={switchLogo} alt="Switch"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample1} />
                                    <Card.Body>
                                        <Card.Title>Watch Dogs: Legion</Card.Title>
                                        <Card.Text>
                                            <p>2021</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div>
                                <Card style={{ width: '18rem', height: '32rem' }}>
                                    <Card.Img variant="top" src={Sample5} />
                                    <Card.Body>
                                        <Card.Title>Prince of Persia: The Sands of Time Remake</Card.Title>
                                        <Card.Text>
                                            <p>2021</p>
                                            <img src={xboxLogo} alt="Xbox"></img><img src={ps4Logo} alt="PS4"></img><img src={windowsLogo} alt="Windows"></img>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        </Carousel>
                        <div className="mb-2" style={{ textAlign: "center", padding: "2em 0 2em 0" }}>
                            <Button variant="primary" size="lg" style={{ borderRadius: "50px", padding: "1em 3em", wordSpacing: "5px", fontWeight: "bold", backgroundColor: "rgb(226, 34, 119)", border: "none" }}>
                                VIEW ALL GAMES
                            </Button>
                        </div>
                    </Container>
                </div></>
            }
        </div >
    )
}

export default SlideShow