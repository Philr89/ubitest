import React from 'react'
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'
import Image1 from '../Assets/LatestNews-Image1.png'
import Image2 from '../Assets/LatestNews-Image2.png'
import Image3 from '../Assets/LatestNews-Image3.png'
import Image4 from '../Assets/LatestNews-Image4.png'
import Image5 from '../Assets/LatestNews-Image5.png'


import { NewsArticle } from "./NewsArticle"
import styled from "styled-components"

const Headline = styled.div`
font-weight: bold;
font-size: 16pt;
`;

const Date = styled.div`
font-size: 12pt
`;

function LatestNews() {
    return (
        <div>

            <div style={{ textAlign: "center", padding: "2em 0 2em 0" }}><h1>LATEST NEWS</h1></div>
            <Nav
                className="justify-content-center"
                activeKey="/home"
                onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}>
                <Nav.Item>
                    <Nav.Link href="/home" style={{ color: "rgb(207, 49, 109)", fontWeight: "bold" }}>ALL NEWS</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-1" style={{ color: "rgb(207, 49, 109)" }}>GHOST RECON BREAKPOINT</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-2" style={{ color: "rgb(207, 49, 109)" }}>WATCH DOGS LEGION</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-3" style={{ color: "rgb(207, 49, 109)" }}>THE DIVISION 2</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-4" style={{ color: "rgb(207, 49, 109)" }}>RAINBOW SIX</Nav.Link>
                </Nav.Item>
            </Nav>

            <div style={{ display: "grid", gridTemplate: "1fr 1fr 1fr / 1fr 1fr", gridGap: "24px", padding: "0 10em" }}>
                <div style={{ gridRowEnd: "span 2" }}>
                    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
                        <div style={{ flex: 1 }}><img src={Image1} alt="Animating the Future" style={{ width: "100%", height: "100%" }} /></div>
                        <Date>October 9, 2020</Date>
                        <Headline>Animating the Future - Developer Interview</Headline>
                    </div>

                </div>
                <NewsArticle image={Image2} date="October 9, 2020" headline="BIPOC of Ubisoft - Fatim Aissatou Diop" isRightText={true} />
                <NewsArticle image={Image3} date="October 8, 2020" headline="For Honor Will Be Playable on Next-Gen Consoles" isRightText={false} />
                <NewsArticle image={Image4} date="October 8, 2020" headline="Rabbids Coding Crashes onto Mobile" isRightText={false} />
                <NewsArticle image={Image5} date="October 6, 2020" headline="Watch Dogs: Legion - New Story Details and Post-Launch Plans Revealed" isRightText={true} />
            </div>


            <div style={{ textAlign: "center", padding: "2em 0 2em 0" }}>
                <Button variant="primary" size="lg" style={{ borderRadius: "50px", padding: "1em 3em", wordSpacing: "5px", fontWeight: "bold", backgroundColor: "rgb(226, 34, 119)", border: "none" }} >
                    ALL NEWS
                </Button></div>
        </div>
    )
}

export default LatestNews