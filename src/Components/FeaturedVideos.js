import React from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Card from 'react-bootstrap/Card'
import VideoSampleImg1 from '../Assets/featured-videos-sample-1.jpg'
import VideoSampleImg2 from '../Assets/featured-videos-sample-2.jpg'
import VideoSampleImg3 from '../Assets/featured-videos-sample-3.jpg'

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    mobile: {
        breakpoint: { max: 864, min: 0 },
        items: 1
    }
};

function VideoModal1(props) {
    return (
        <Modal
            {...props}
            size="m"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/E_V-YQW3Gos" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Modal>
    );
}

function VideoModal2(props) {
    return (
        <Modal
            {...props}
            size="m"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/d0epU_G4GWY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Modal>
    );
}

function VideoModal3(props) {
    return (
        <Modal
            {...props}
            size="m"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/ipRaFjK7ShQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Modal>
    );
}

function FeaturedVideos() {
    const [modalShow1, setModalShow1] = React.useState(false);
    const [modalShow2, setModalShow2] = React.useState(false);
    const [modalShow3, setModalShow3] = React.useState(false);
    return (
        <div style={{ backgroundColor: "rgb(239, 245, 243)" }}>
            {
                <><div style={{ textAlign: "center", padding: "2em 0 2em 0" }}><h1>FEATURED VIDEOS</h1></div><div className="mb-2">
                    <Carousel responsive={responsive}>
                        <Card style={{ width: '30rem', marginLeft: "10em" }} onClick={() => setModalShow1(true)}>
                            <Card.Img variant="top" src={VideoSampleImg1} />
                        </Card>
                        <Card style={{ width: '30rem', marginLeft: "8em" }} onClick={() => setModalShow2(true)}>
                            <Card.Img variant="top" src={VideoSampleImg2} />
                        </Card>
                        <Card style={{ width: '30rem' }} onClick={() => setModalShow3(true)}>
                            <Card.Img variant="top" src={VideoSampleImg3} />
                        </Card>
                    </Carousel>
                    <VideoModal1
                        show={modalShow1}
                        onHide={() => setModalShow1(false)}
                    />
                    <VideoModal2
                        show={modalShow2}
                        onHide={() => setModalShow2(false)}
                    />
                    <VideoModal3
                        show={modalShow3}
                        onHide={() => setModalShow3(false)}
                    />
                    <div style={{ textAlign: "center", padding: "2em 0 2em 0" }}>
                        <Button variant="primary" size="lg" style={{ borderRadius: "50px", padding: "1em 3em", wordSpacing: "5px", fontWeight: "bold", backgroundColor: "rgb(226, 34, 119)", border: "none" }}>
                            VIEW ALL TRAILERS
                        </Button>
                    </div>
                </div></>
            }
        </div>
    )
}

export default FeaturedVideos